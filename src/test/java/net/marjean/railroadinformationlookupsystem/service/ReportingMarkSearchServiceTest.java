package net.marjean.railroadinformationlookupsystem.service;

import net.marjean.railroadinformationlookupsystem.domain.ReportingMark;
import net.marjean.railroadinformationlookupsystem.mapper.ReportingMarkMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class ReportingMarkSearchServiceTest {

    private static ReportingMarkSearchService testUnit;
    private static ReportingMarkMapper mapper;

    private static Map<String, ReportingMark> mockData;

    @BeforeClass
    public static void setup(){
        //mapper = new MockReportingMarkMapper();

        mockData = new HashMap<>();

        ReportingMark cnw = new ReportingMark();
        cnw.setMark("CNW");
        cnw.setName("Chicago and North Western Railway");
        cnw.setId(1205);
        mockData.put(cnw.getMark(), cnw);

        ReportingMark cnws = new ReportingMark();
        cnws.setMark("CNWS");
        cnws.setName("Chicago and North Western Railway");
        cnws.setId(1206);
        mockData.put(cnws.getMark(), cnws);

        ReportingMark cnwx = new ReportingMark();
        cnwx.setMark("CNWX");
        cnwx.setName("Canadian Wheat Board");
        cnwx.setId(1207);
        mockData.put(cnwx.getMark(), cnwx);

        ReportingMark cnwz = new ReportingMark();
        cnwz.setMark("CNWZ");
        cnwz.setName("Chicago and North Western Railway");
        cnwz.setId(1208);
        mockData.put(cnwz.getMark(), cnwz);

        ReportingMark ag = new ReportingMark();
        ag.setMark("AG");
        ag.setName("Abbeville-Grimes Railway");
        ag.setId(382);
        mockData.put(ag.getMark(), ag);



        ReportingMark test2 = new ReportingMark();
        test2.setMark("ATSF");
        test2.setName("Atchison, Topeka, and Santa Fe");
        mockData.put("ATSF", test2);

        ReportingMark test3 = new ReportingMark();
        test3.setMark("CN");
        test3.setName("Canadian National");
        mockData.put("CN", test3);

        mapper = Mockito.mock(ReportingMarkMapper.class);
        testUnit = new ReportingMarkSearchService(mapper);
    }

    @Before
    public void reset(){
        Mockito.reset(mapper);

        Mockito.when(mapper.allReportingMarks()).thenReturn(new ArrayList<>(mockData.values()));
        Mockito.when(mapper.reportingMarkLike(Mockito.matches("CNW"), ArgumentMatchers.isNull())).thenReturn(Collections.singletonList(mockData.get("CNW")));
        Mockito.when(mapper.reportingMarkLike(Mockito.matches("CNW%"), ArgumentMatchers.isNull())).thenReturn(Arrays.asList(mockData.get("CNW"), mockData.get("CNWS"), mockData.get("CNWX"), mockData.get("CNWZ")));
        Mockito.when(mapper.reportingMarkLike(ArgumentMatchers.isNull(), Mockito.eq("CHICAGO%AND%NORTH%WESTERN%RAILWAY"))).thenReturn(Arrays.asList(mockData.get("CNW"), mockData.get("CNWS"), mockData.get("CNWZ")));
        Mockito.when(mapper.reportingMarkLike(ArgumentMatchers.isNull(), Mockito.eq("ABBEVILLE%GRIMES%RAILWAY"))).thenReturn(Collections.singletonList(mockData.get("AG")));
    }



    //TODO these are really more like end to end tests.  Should think about replacing with calls to Mockito.verify()

    @Test
    public void testAllReportingMarks() {

        List<ReportingMark> result = testUnit.allReportingMarks();

        assertNotNull(result);
        assertEquals(mockData.size(), result.size());
    }

    @Test
    public void testReportingMarkSearch_exactMark() {
        List<ReportingMark> result = testUnit.reportingMarkSearch("CNW", null);

        assertNotNull(result);
        assertEquals(1, result.size());

        ReportingMark rm = result.get(0);

        assertEquals("CNW", rm.getMark());
        assertEquals("Chicago and North Western Railway", rm.getName());
        assertEquals(1205, rm.getId());



    }

    @Test
    public void testReportingMarkSearch_wildcardMark() {
        List<ReportingMark> result = testUnit.reportingMarkSearch("CNW%", null);

        // Should return 4 marks.  CNW, CNWS, CNWX, and CNWZ.  Order not guaranteed.

        assertNotNull(result);
        assertEquals(4, result.size());

        boolean cnw = false;
        boolean cnws = false;
        boolean cnwx = false;
        boolean cnwz = false;

        for(ReportingMark rm : result) {
            if(rm.getMark().equals("CNW")) {
                assertEquals("CNW", rm.getMark());
                assertEquals("Chicago and North Western Railway", rm.getName());
                assertEquals(1205, rm.getId());
                cnw = true;
            }
            if(rm.getMark().equals("CNWS")) {
                assertEquals("CNWS", rm.getMark());
                assertEquals("Chicago and North Western Railway", rm.getName());
                assertEquals(1206, rm.getId());
                cnws = true;
            }
            if(rm.getMark().equals("CNWX")) {
                assertEquals("CNWX", rm.getMark());
                assertEquals("Canadian Wheat Board", rm.getName());
                assertEquals(1207, rm.getId());
                cnwx = true;
            }
            if(rm.getMark().equals("CNWZ")) {
                assertEquals("CNWZ", rm.getMark());
                assertEquals("Chicago and North Western Railway", rm.getName());
                assertEquals(1208, rm.getId());
                cnwz = true;
            }
        }

        assertTrue(cnw & cnws & cnwx & cnwz);
    }

    @Test
    public void testReportingMarkSearch_exactName() {
        List<ReportingMark> result = testUnit.reportingMarkSearch(null, "Chicago and North Western Railway");

        assertNotNull(result);
        assertEquals(3, result.size());

        boolean cnw = false;
        boolean cnws = false;
        boolean cnwz = false;

        for(ReportingMark rm : result) {
            if(rm.getMark().equals("CNW")) {
                assertEquals("CNW", rm.getMark());
                assertEquals("Chicago and North Western Railway", rm.getName());
                assertEquals(1205, rm.getId());
                cnw = true;
            }
            if(rm.getMark().equals("CNWS")) {
                assertEquals("CNWS", rm.getMark());
                assertEquals("Chicago and North Western Railway", rm.getName());
                assertEquals(1206, rm.getId());
                cnws = true;
            }
            if(rm.getMark().equals("CNWZ")) {
                assertEquals("CNWZ", rm.getMark());
                assertEquals("Chicago and North Western Railway", rm.getName());
                assertEquals(1208, rm.getId());
                cnwz = true;
            }
        }
        assertTrue(cnw & cnws & cnwz);
    }

    @Test
    public void testReportingMarkSearch_specialCharsName() {
        List<ReportingMark> result = testUnit.reportingMarkSearch(null, "ABBEVILLE-GRIMES RAILWAY");

        assertNotNull(result);
        assertEquals(1, result.size());
    }


}
