package net.marjean.railroadinformationlookupsystem.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.marjean.railroadinformationlookupsystem.domain.ReportingMark;

@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("application-test.properties")
@MybatisTest
public class ReportingMarkMapperTest {
	
	@Autowired
	ReportingMarkMapper testThing;
	
//	select
//	*
//	from
//	reporing_mark

	@Test
	public void testAllReportingMarks() {
		
		List<ReportingMark> result = testThing.allReportingMarks();
		
		assertNotNull(result);
		assertEquals(3876, result.size());
	}
	
//	select
//	*
//	from
//	reporting_mark rm
//	where
//	rm.mark like 'CNW'
	@Test
	public void testReportingMarkLike_exactMark() {
		List<ReportingMark> result = testThing.reportingMarkLike("CNW", null);//testUnit.reportingMarkSearch("CNW");
		
		assertNotNull(result);
		assertEquals(1, result.size());
		
		ReportingMark rm = result.get(0);
		
		assertEquals("CNW", rm.getMark());
		assertEquals("Chicago and North Western Railway", rm.getName());
		assertEquals(1205, rm.getId());	
		
	}
	

	//select
	//*
	//from
	//reporting_mark rm
	//where
	//rm.mark like 'CNW%'
	@Test
	public void testReportingMarkLike_wildcardMark() {
		List<ReportingMark> result = testThing.reportingMarkLike("CNW%", null);//testUnit.reportingMarkSearch("CNW%");
		
		// Should return 4 marks.  CNW, CNWS, CNWX, and CNWZ.  Order not guaranteed.
		
		assertNotNull(result);
		assertEquals(4, result.size());
		
		boolean cnw = false;
		boolean cnws = false;
		boolean cnwx = false;
		boolean cnwz = false;
		
		for(ReportingMark rm : result) {
			if(rm.getMark().equals("CNW")) {
				assertEquals("CNW", rm.getMark());
				assertEquals("Chicago and North Western Railway", rm.getName());
				assertEquals(1205, rm.getId());
				cnw = true;
			}
			if(rm.getMark().equals("CNWS")) {
				assertEquals("CNWS", rm.getMark());
				assertEquals("Chicago and North Western Railway", rm.getName());
				assertEquals(1206, rm.getId());
				cnws = true;
			}
			if(rm.getMark().equals("CNWX")) {
				assertEquals("CNWX", rm.getMark());
				assertEquals("Canadian Wheat Board", rm.getName());
				assertEquals(1207, rm.getId());
				cnwx = true;
			}
			if(rm.getMark().equals("CNWZ")) {
				assertEquals("CNWZ", rm.getMark());
				assertEquals("Chicago and North Western Railway", rm.getName());
				assertEquals(1208, rm.getId());
				cnwz = true;
			}
		}
		
		assertTrue(cnw & cnws & cnwx & cnwz);
	}
	

	//select
	//*
	//from
	//reporting_mark rm
	//where
	//rm.name like 'Chicago and North Western Railway'
	@Test
	public void testReportingMarkLike_exactName() {
		List<ReportingMark> result = testThing.reportingMarkLike(null, "CHICAGO AND NORTH WESTERN RAILWAY");
		
		assertNotNull(result);
		assertEquals(3, result.size());
		
		boolean cnw = false;
		boolean cnws = false;
		boolean cnwz = false;
		
		for(ReportingMark rm : result) {
			if(rm.getMark().equals("CNW")) {
				assertEquals("CNW", rm.getMark());
				assertEquals("Chicago and North Western Railway", rm.getName());
				assertEquals(1205, rm.getId());
				cnw = true;
			}
			if(rm.getMark().equals("CNWS")) {
				assertEquals("CNWS", rm.getMark());
				assertEquals("Chicago and North Western Railway", rm.getName());
				assertEquals(1206, rm.getId());
				cnws = true;
			}
			if(rm.getMark().equals("CNWZ")) {
				assertEquals("CNWZ", rm.getMark());
				assertEquals("Chicago and North Western Railway", rm.getName());
				assertEquals(1208, rm.getId());
				cnwz = true;
			}
		}
		assertTrue(cnw & cnws & cnwz);
	}
	
	//NOTE: MySQL like isn't case sensitive, but H2 is.
	// Even putting H2 into MySQL compatibility mode doesn't change it.
//	select
//	*
//	from
//	reporting_mark rm
//	where
//	rm.name like 'CHICAGO AND%'
	@Test
	public void testReportingMarkLike_wildcardName() {
		List<ReportingMark> result = testThing.reportingMarkLike(null, "CHICAGO AND%");
		
		assertNotNull(result);
		assertEquals(6,result.size());
		
		boolean cei = false;
		boolean cim = false;
		boolean cnw = false;
		boolean cnws = false;
		boolean cnwz = false;
		boolean cwi = false;
		
		for(ReportingMark rm : result) {
			if(rm.getMark().equals("CEI")) {
				assertEquals("CEI", rm.getMark());
				assertEquals("Chicago and Eastern Illinois Railroad", rm.getName());
				assertEquals(1034, rm.getId());
				cei = true;
			}
			if(rm.getMark().equals("CIM")) {
				assertEquals("CIM", rm.getMark());
				assertEquals("Chicago and Illinois Midland Railway", rm.getName());
				assertEquals(1109, rm.getId());
				cim = true;
			}
			if(rm.getMark().equals("CNW")) {
				assertEquals("CNW", rm.getMark());
				assertEquals("Chicago and North Western Railway", rm.getName());
				assertEquals(1205, rm.getId());
				cnw = true;
			}
			if(rm.getMark().equals("CNWS")) {
				assertEquals("CNWS", rm.getMark());
				assertEquals("Chicago and North Western Railway", rm.getName());
				assertEquals(1206, rm.getId());
				cnws = true;
			}
			if(rm.getMark().equals("CNWZ")) {
				assertEquals("CNWZ", rm.getMark());
				assertEquals("Chicago and North Western Railway", rm.getName());
				assertEquals(1208, rm.getId());
				cnwz = true;
			}
			if(rm.getMark().equals("CWI")) {
				assertEquals("CWI", rm.getMark());
				assertEquals("Chicago and Western Indiana Railroad", rm.getName());
				assertEquals(1383, rm.getId());
				cwi = true;
			}
		}
		
		assertTrue(cei & cim & cnw & cnws & cnwz & cwi);
	}
	
	@Test
	public void testReportingMarkLike_nullParams(){
		
		List<ReportingMark> result = testThing.reportingMarkLike(null, null);
		List<ReportingMark> comparison = testThing.allReportingMarks();
		
		//This might be another reason to implement ReportingMark.equals()...
		assertEquals(comparison.size(), result.size());
		
	}

}
