package net.marjean.railroadinformationlookupsystem.mapper;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.marjean.railroadinformationlookupsystem.domain.RailsRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("application-test.properties")
@MybatisTest
public class SearchRequestMapperTest {
	
	@Autowired
	private SearchRequestMapper searchMapper;
	
	@Test
	public void testSearchRequest_insert() {
		
		List<RailsRequest> res1 = searchMapper.allSearchRequests();
		
		assertEquals(0, res1.size());
		
		RailsRequest insert = new RailsRequest();
		insert.setMark("WHO");
		insert.setName("WHAT");
		insert.setMethod("GET");
		insert.setOrigin("Somewhere");
		insert.setQueryString("Yes, there will be one");
		insert.setReferrer("Some guy");
		insert.setRemoteAddr("Home");
		insert.setUri("eeew");
		insert.setUserAgent("Space Bison 1.0");
		
		searchMapper.writeSearchRequest(insert);
		
		List<RailsRequest> res2 = searchMapper.allSearchRequests();
		
		assertEquals(1, res2.size());
		
		
	}

}
