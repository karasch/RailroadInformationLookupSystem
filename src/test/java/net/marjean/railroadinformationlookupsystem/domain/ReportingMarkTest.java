/**
 * 
 */
package net.marjean.railroadinformationlookupsystem.domain;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

/**
 * @author karasch
 *
 */
public class ReportingMarkTest {
	
	@Test
	public void testCompanyURL() {
		ReportingMark tester = new ReportingMark();
		tester.setCompanyUrl("http://railpictures.net");
		assertEquals(tester.getCompanyUrl(), "http://railpictures.net");
	}
	
	@Test
	public void testCreated() {
		ReportingMark tester = new ReportingMark();
		Date timestamp = new Date();
		tester.setCreated(timestamp);
		
		assertEquals(tester.getCreated(), timestamp);
	}
	
	@Test
	public void testId() {
		ReportingMark tester = new ReportingMark();
		tester.setId(13);
		assertEquals(tester.getId(), 13);
	}
	
	@Test
	public void testLastUpdated() {
		ReportingMark tester = new ReportingMark();
		Date timestamp = new Date();
		tester.setLastUpdated(timestamp);
		assertEquals(tester.getLastUpdated(), timestamp);
	}
	
	@Test
	public void testMark() {
		ReportingMark tester = new ReportingMark();
		tester.setMark("CNW");
		assertEquals(tester.getMark(), "CNW");
	}
	
	@Test
	public void testName() {
		ReportingMark tester = new ReportingMark();
		tester.setName("Wisconsin & Southern Railroad");
		assertEquals(tester.getName(), "Wisconsin & Southern Railroad");
	}
	
	@Test
	public void testNotes() {
		ReportingMark tester = new ReportingMark();
		tester.setNotes("This is a string.");
		assertEquals(tester.getNotes(), "This is a string.");
	}
	
	@Test
	public void testWikiURL() {
		ReportingMark tester = new ReportingMark();
		tester.setWikiUrl("http://en.wikipedia.org");
		assertEquals(tester.getWikiUrl(), "http://en.wikipedia.org");
	}
	
	@Test
	public void testYearsActive() {
		ReportingMark tester = new ReportingMark();
		tester.setYearsActive("1890-2005");
		assertEquals(tester.getYearsActive(), "1890-2005");
	}
	


}
