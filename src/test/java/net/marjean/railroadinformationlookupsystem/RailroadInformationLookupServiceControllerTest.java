package net.marjean.railroadinformationlookupsystem;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import net.marjean.railroadinformationlookupsystem.mapper.ReportingMarkMapper;
import net.marjean.railroadinformationlookupsystem.mapper.SearchRequestMapper;
import net.marjean.railroadinformationlookupsystem.service.ReportingMarkSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import net.marjean.railroadinformationlookupsystem.domain.ReportingMark;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(RailroadInformationLookupServiceController.class)
public class RailroadInformationLookupServiceControllerTest {

	@Autowired
	private MockMvc mockMvc;

	// Mock our service so we can control what it returns
	@MockBean
	private ReportingMarkSearchService mockService;

	// Since we're effectively spinning up the service, we also need to mock the other required components.
	@MockBean
	private RailroadInformationLookupInterceptor mockInterceptor;
	@MockBean
	private ReportingMarkMapper mockMapper;
	@MockBean
	private SearchRequestMapper mockSearchMapper;


	@Test
	public void test_allReportingMarks() throws Exception{

		ArrayList<ReportingMark> serviceList = new ArrayList<>();
		ReportingMark rm = new ReportingMark();
		serviceList.add(rm);


		Mockito.when(mockService.allReportingMarks()).thenReturn(serviceList);
		Mockito.when(mockInterceptor.preHandle(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(true);

		// Verify that an /allreportingmarks endpoint exists
		mockMvc.perform(get("/allreportingmarks")).andDo(print()).andExpect(status().isOk());

		// Verify that calling the endpoint calls the underlying service.
		Mockito.verify(mockService).allReportingMarks();
		Mockito.verify(mockInterceptor).preHandle(Mockito.any(), Mockito.any(), Mockito.any());

		Mockito.reset(mockService, mockInterceptor);
	}

	@Test
	public void test_reporingMarkSearch() throws Exception{

		String searchName = "Bob";
		String searchMark = "BOBX";

		ArrayList<ReportingMark> serviceList = new ArrayList<>();
		ReportingMark rm = new ReportingMark();
		serviceList.add(rm);

		ArgumentCaptor<String> markCaptor = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);

		Mockito.when(mockService.reportingMarkSearch(Mockito.anyString(), Mockito.anyString())).thenReturn(serviceList);
		Mockito.when(mockInterceptor.preHandle(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(true);

		// Verify that the endpoint exists
		mockMvc.perform(get("/reportingmarksearch").param("name", searchName).param("mark", searchMark)).andExpect(status().isOk());

		// Verify that the endpoint calls the expected service
		Mockito.verify(mockService).reportingMarkSearch(markCaptor.capture(), nameCaptor.capture());
		Mockito.verify(mockInterceptor).preHandle(Mockito.any(), Mockito.any(), Mockito.any());

		// Verify that the endpoint passes the expected parameters.
		assertEquals(searchName, nameCaptor.getValue());
		assertEquals(searchMark, markCaptor.getValue());

		Mockito.reset(mockService, mockInterceptor);

	}



}
