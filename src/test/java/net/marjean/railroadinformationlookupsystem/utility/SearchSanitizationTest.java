package net.marjean.railroadinformationlookupsystem.utility;

import org.junit.Test;

import static org.junit.Assert.*;

public class SearchSanitizationTest {

    @Test
    public void testRemoveNotLetters_null() {
        assertNull(SearchSanitization.removeNotLetters(null));
    }

    @Test
    public void testRemoveNotLetters_specialCharacters() {
        assertEquals("CNW", SearchSanitization.removeNotLetters("#CNW??@)"));
        assertEquals("CN", SearchSanitization.removeNotLetters("13C&&&N"));
    }

    @Test
    public void testRemoveNotLetters_percent() {
        assertEquals("CN%", SearchSanitization.removeNotLetters("CN%"));
        assertEquals("BN%SF%", SearchSanitization.removeNotLetters("BN%SF%"));
    }

    @Test
    public void testRemoveNotLetters_spaces() {
        assertNotEquals("CNW", SearchSanitization.removeNotLetters("C N W"));
        assertEquals("C N W", SearchSanitization.removeNotLetters("C N W"));

    }

    @Test
    public void testReplaceSpecialCharactersWithPercent_null() {
        assertNull(SearchSanitization.replaceSpecialCharactersWithPercent(null));
    }

    @Test
    public void testReplaceSpecialCharactersWithPercent_midword() {
        assertNotEquals("Wi,scon&sin", SearchSanitization.replaceSpecialCharactersWithPercent("Wi,scon&sin"));
        assertEquals("Wi%scon%sin", SearchSanitization.replaceSpecialCharactersWithPercent("Wi,scon&sin"));
    }

    @Test
    public void testReplaceSpecialCharactersWithPercent_multipleConsecutive() {
        assertEquals("Wis%consin", SearchSanitization.replaceSpecialCharactersWithPercent("Wis444consin"));
    }

    @Test
    public void testReplaceSpecialCharactersWithPercent_spaces() {
        assertEquals("Wis%consin", SearchSanitization.replaceSpecialCharactersWithPercent("Wis consin"));
    }

}
