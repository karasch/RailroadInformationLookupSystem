SET MODE MySQL
;

CREATE TABLE reporting_mark
(
   id int PRIMARY KEY NOT NULL,
   searchmark varchar(5) NOT NULL,
   searchname varchar(255) NOT NULL,
   mark varchar(5) NOT NULL,
   name varchar(255) NOT NULL,
   created_date timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
   last_updated_date timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
   years_active varchar(255),
   company_url varchar(255),
   wiki_url varchar(255),
   notes longtext
)
;

CREATE TABLE search_request (
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	request_datetime timestamp,
	referrer varchar(255),
	user_agent varchar(255),
	origin varchar(255),
	method varchar(10),
	uri varchar(255),
	remote_addr varchar(255),
	query_string varchar(255),
	mark varchar(10),
	name varchar(255)
)
;

-- This doesn't seem to work in H2.
--CREATE UNIQUE INDEX PRIMARY ON reporting_mark(id)
--;
