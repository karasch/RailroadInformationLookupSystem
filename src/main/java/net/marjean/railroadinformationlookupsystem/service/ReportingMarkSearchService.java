package net.marjean.railroadinformationlookupsystem.service;

import net.marjean.railroadinformationlookupsystem.domain.ReportingMark;
import net.marjean.railroadinformationlookupsystem.mapper.ReportingMarkMapper;
import net.marjean.railroadinformationlookupsystem.utility.SearchSanitization;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportingMarkSearchService {

    private final ReportingMarkMapper reportingMarkMapper;

    public ReportingMarkSearchService(ReportingMarkMapper mapper){
        reportingMarkMapper = mapper;
    }

    public List<ReportingMark> allReportingMarks(){
        return reportingMarkMapper.allReportingMarks();
    }

    public List<ReportingMark> reportingMarkSearch(String markParam, String nameParam){
        String queryMark = null;
        if(markParam != null)
            queryMark = SearchSanitization.removeNotLetters(markParam).toUpperCase();

        String queryName = null;
        if(nameParam != null)
            queryName = SearchSanitization.replaceSpecialCharactersWithPercent(nameParam).toUpperCase();

        return reportingMarkMapper.reportingMarkLike(queryMark, queryName);
    }

}
