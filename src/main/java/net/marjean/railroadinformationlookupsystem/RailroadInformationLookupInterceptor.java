package net.marjean.railroadinformationlookupsystem;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import net.marjean.railroadinformationlookupsystem.domain.RailsRequest;
import net.marjean.railroadinformationlookupsystem.mapper.SearchRequestMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class RailroadInformationLookupInterceptor implements HandlerInterceptor {

	@SuppressFBWarnings(value = "EI_EXPOSE_REP2", justification = "This is allocated by Spring.  It'll be fine.")
	private final SearchRequestMapper searchMapper;

	public RailroadInformationLookupInterceptor(SearchRequestMapper mapper){
		searchMapper = mapper;
	}
	
	private Logger logger = LogManager.getLogger(RailroadInformationLookupInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		
		//TODO Y U NO RUN WITH DEBUG?!
		//System.err.println("Thing has been called!");
		
		HttpServletRequest requestCacheWrapperObject = new ContentCachingRequestWrapper(request);
		
		RailsRequest rr = new RailsRequest(requestCacheWrapperObject);
		logger.info(rr.toString());
		searchMapper.writeSearchRequest(rr);
		
		/*
		Enumeration<String>  headers = requestCacheWrapperObject.getHeaderNames();
		while(headers.hasMoreElements()) {
			String header = headers.nextElement();
			logger.info("Request header: " + header + " = " + requestCacheWrapperObject.getHeader(header));
		}
		
		*/
		
		
		return true;
	}

}
