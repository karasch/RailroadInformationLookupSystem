package net.marjean.railroadinformationlookupsystem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import net.marjean.railroadinformationlookupsystem.domain.ReportingMark;

public interface ReportingMarkMapper {
	
	public List<ReportingMark> allReportingMarks();
	
	public List<ReportingMark> reportingMarkLike(@Param("mark")String mark, @Param("name")String name);

}
