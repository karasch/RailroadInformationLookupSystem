package net.marjean.railroadinformationlookupsystem.mapper;

import java.util.List;

import net.marjean.railroadinformationlookupsystem.domain.RailsRequest;

public interface SearchRequestMapper {
	
	/**
	 * I don't plan to use this in production, but I can't think of
	 * a way to test without this.
	 * 
	 * @return All the requests in the table
	 */
	public List<RailsRequest> allSearchRequests();
	
	public void writeSearchRequest(RailsRequest request);

}
