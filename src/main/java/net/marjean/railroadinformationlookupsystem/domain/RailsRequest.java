package net.marjean.railroadinformationlookupsystem.domain;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * Add a couple things to HttpRequestData that only this application cares about.
 * 
 * @author karasch
 *
 */
public class RailsRequest extends HttpRequestData {
	
	private String mark;
	private String name;
	
	public RailsRequest() { super(); }
	
	public RailsRequest(HttpServletRequest request) {
		super(request);
		
		mark = request.getParameter("mark");
		name = request.getParameter("name");
	}
	
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(super.toString());
		
		sb.append(" Mark:");
		sb.append(getMark());
		
		sb.append(" Name:");
		sb.append(getName());
	
		return sb.toString();
	}
	

}
