package net.marjean.railroadinformationlookupsystem.domain;

import java.util.Date;

/*
 
CREATE TABLE reporting_mark
(
id INT AUTO_INCREMENT,
mark VARCHAR(5) NOT NULL,
name VARCHAR(255) NOT NULL,
created_date DATETIME NOT NULL DEFAULT NOW(),
last_updated_date TIMESTAMP,
years_active VARCHAR(255),
company_url VARCHAR(255),
wiki_url VARCHAR(255),
notes TEXT,
PRIMARY KEY(id)
)
 */

public class ReportingMark {
	
	private int id;
	private String mark;
	private String name;
	private Date created;
	private Date lastUpdated;
	private String yearsActive;
	private String wikiUrl;
	private String companyUrl;
	private String notes;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreated() {
		if(created == null) return null;
		return new Date(created.getTime());
	}
	public void setCreated(Date created) {
		this.created = new Date(created.getTime());
	}
	public Date getLastUpdated() {
		if(lastUpdated == null) return null;
		return new Date(lastUpdated.getTime());
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = new Date(lastUpdated.getTime());
	}
	public String getYearsActive() {
		return yearsActive;
	}
	public void setYearsActive(String yearsActive) {
		this.yearsActive = yearsActive;
	}
	public String getWikiUrl() {
		return wikiUrl;
	}
	public void setWikiUrl(String wikiUrl) {
		this.wikiUrl = wikiUrl;
	}
	public String getCompanyUrl() {
		return companyUrl;
	}
	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}

}
