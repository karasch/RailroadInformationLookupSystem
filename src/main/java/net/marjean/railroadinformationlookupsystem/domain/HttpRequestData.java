package net.marjean.railroadinformationlookupsystem.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * Retrieve some useful information from an HTTP request, and store it
 * in a data object suitable for writing to a database.
 * 
 * Remember that reading an HttpServletRequest normally marks it as read.
 * 
 * @see org.springframework.web.util.ContentCachingRequestWrapper
 * 
 * @author karasch
 *
 */
public class HttpRequestData {
	
	private String referrer;
	private String userAgent;
	private String origin;
	private String method;
	private String uri;
	private String remoteAddr;
	private String queryString;
	private Date requestDate;
	
	public HttpRequestData() {}
	
	public HttpRequestData(HttpServletRequest request) {
		referrer = request.getHeader("referer");// Yup.  They spelled it wrong.
		userAgent = request.getHeader("user-agent");
		origin = request.getHeader("origin");
		method = request.getMethod();
		uri = request.getRequestURI();
		remoteAddr = request.getRemoteAddr();
		setQueryString(request.getQueryString());
		requestDate = new Date();
	}
	
	public String getReferrer() {
		return referrer;
	}
	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getRemoteAddr() {
		return remoteAddr;
	}
	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}
	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	public Date getRequestDate() {
		if (requestDate == null) return null;
		return new Date(requestDate.getTime());
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = new Date(requestDate.getTime());
	}
	
	@Override
	public String toString() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		StringBuilder sb = new StringBuilder();
		
		sb.append(sdf.format(getRequestDate()));
		
		sb.append(" referrer: ");
		sb.append(getReferrer());
		
		sb.append(" user-agent: ");
		sb.append(getUserAgent());
		
		sb.append(" origin: ");
		sb.append(getOrigin());
		
		sb.append(" method: ");
		sb.append(getMethod());
		
		sb.append(" URI:");
		sb.append(getUri());
		
		sb.append(" Remote Addr: ");
		sb.append(getRemoteAddr());
		
		sb.append(" Query String: " );
		sb.append(getQueryString());
		
		return sb.toString();
	}

}
