package net.marjean.railroadinformationlookupsystem;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class RailroadInformationLookupMVCConfig implements WebMvcConfigurer {

	private final RailroadInformationLookupInterceptor interceptor;

	public RailroadInformationLookupMVCConfig(RailroadInformationLookupInterceptor interceptor){
		this.interceptor = interceptor;
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(interceptor).addPathPatterns("/**");
		//registry.addInterceptor(interceptor).addPathPatterns("/reportingmarksearch");
		// ... I have no idea how path patterns work.

	}
}
