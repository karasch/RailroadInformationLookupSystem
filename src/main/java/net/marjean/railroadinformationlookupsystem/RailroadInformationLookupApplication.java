package net.marjean.railroadinformationlookupsystem;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//putting some TODOs here that don't necessarily belong anywhere else.
//In no particular order
//TODO find a way to have the service parameters anded or ored based on another input
//TODO add railroad history / owning railroads
//TODO populate more fields.
//TODO logging
//TODO something about 
/* WARN: Establishing SSL connection without server's identity verification is not recommended. According to MySQL 5.5.45+, 5.6.26+ and 5.7.6+ requirements SSL connection must be established by default if explicit option isn't set. For compliance with existing applications not using SSL the verifyServerCertificate property is set to 'false'. 
 *
 */

@SpringBootApplication
@MapperScan("net.marjean.railroadinformationlookupsystem.mapper")
public class RailroadInformationLookupApplication {

	public static void main(String[] args) {
		SpringApplication.run(RailroadInformationLookupApplication.class, args);
	}
}
