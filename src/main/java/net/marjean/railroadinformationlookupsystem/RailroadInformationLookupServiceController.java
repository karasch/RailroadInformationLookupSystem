package net.marjean.railroadinformationlookupsystem;

import java.util.List;

import net.marjean.railroadinformationlookupsystem.service.ReportingMarkSearchService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.marjean.railroadinformationlookupsystem.domain.ReportingMark;

@RestController
public class RailroadInformationLookupServiceController {

	private final ReportingMarkSearchService searchService;

	public RailroadInformationLookupServiceController(ReportingMarkSearchService service){
		searchService = service;
	}
	
	private final Logger logger = LogManager.getLogger(RailroadInformationLookupServiceController.class);

	@GetMapping("/allreportingmarks")
	public List<ReportingMark> allReportingMarks(){
		logger.info("Received request for allReportingMarks");
		return searchService.allReportingMarks();
	}
	

	//TODO find a way to test that parameters are optional in the service?
	@GetMapping("/reportingmarksearch")
	public List<ReportingMark> reportingMarkSearch(@RequestParam(name="mark", required=false) String markParam, @RequestParam(name="name", required=false) String nameParam){

		//Remove new line characters from input before logging.
		String logMark = (markParam == null)? "null" : markParam.replaceAll("[\r\n]", "");
		String logName = (nameParam == null)? "null" : nameParam.replaceAll("[\r\n]", "");
		logger.info("Received request for reportingmarks - mark: " + logMark + " name: " + logName);

		return searchService.reportingMarkSearch(markParam, nameParam);
	}
	


}
