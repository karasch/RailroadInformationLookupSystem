package net.marjean.railroadinformationlookupsystem.utility;

public class SearchSanitization {

    //An instance of this class would be meaningless.
    private SearchSanitization(){}

    /**
     * Return a string that contains only letters and the % character
     * Other characters are removed.
     * Will return null if passed null.
     *
     * @param input
     * @return
     */
    public static String removeNotLetters(String input) {
        String retVal = null;

        if(input != null)
            retVal = input.replaceAll("[^A-Za-z% ]", "");

        return retVal;
    }

    /**
     * Return a string that contains only letters and the % character.
     * Other characters are replaced with %
     * Will return null if passed null.
     *
     * @param input
     * @return
     */
    public static String replaceSpecialCharactersWithPercent(String input) {
        String retVal = null;

        if(input != null)
            retVal = input.replaceAll("[^A-Za-z%]+", "%");

        return retVal;
    }
}
