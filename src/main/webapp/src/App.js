import React, { Component } from 'react';
import axios from 'axios';
//import logo from './logo.svg';
import {Button, Form, FormGroup, FormControl, FormLabel} from 'react-bootstrap';
//import RailroadEntryList from './pagedraw/railroadentrylist';
import RailroadEntry from './pagedraw/railroadentry';
//import InputForm from './pagedraw/inputform';

import './App.css';

class RailroadList extends React.Component {
	render(){

		const railroadData = this.props.data;
		let railroadList = railroadData.map((rr) => <RailroadEntry name={rr.name} mark={rr.mark} key={rr.id} />);
		if(railroadData === null || railroadData.length === 0){
			railroadList = "No results to display.";
		}
		return(
			<div>{railroadList}</div>
		);
	}
}

/*
class Railroad extends React.Component {
	render(){
		return(
			<div>
				<ul>
					<li><b>Reporting Mark</b> {this.props.mark}</li>
					<li><b>Railroad Name</b> {this.props.name}</li>
				</ul>
			</div>
		);
	}
}
*/

//const mockRRList = [{id: 1, name: "Ann Arbor Railroad", mark: "AA"}, {id: 2, name: "Association of American Railroads", mark: "AAR"}];

//const baseURL = "http://192.168.1.6:8080/RaILS-0.0.1-SNAPSHOT";
//const baseURL = "http://127.0.0.1:8080"
const baseURL = "";
//const baseURL = "http://app.rail-fan.com"; 

class App extends Component {
	constructor(){
		super();
		this.state = {data: [],
										markSearch: null,
									};
		this.handleMarkChange = this.handleMarkChange.bind(this);
		this.handleNameChange = this.handleNameChange.bind(this);
		this.doSearch = this.doSearch.bind(this);
	}

	handleMarkChange(event){
		this.setState({markSearch: event.target.value});
	}

	handleNameChange(event){
		this.setState({nameSearch: event.target.value});
	}

	doSearch(){

		let searchURI = baseURL + "/reportingmarksearch?";

		if(this.state.markSearch != null && this.state.markSearch !== ""){
			searchURI = searchURI + "mark=" + this.state.markSearch + "&";
		}

		if(this.state.nameSearch != null && this.state.nameSearch !== ""){
			searchURI = searchURI + "name=" + this.state.nameSearch;
		}

		console.log("Search URI: " + searchURI);

		searchURI = encodeURI(searchURI);

		console.log("EncodedURI: " + searchURI);
		axios.get(searchURI).then((response) => this.setState({data: response.data}));
	}

  render() {
    return (
      <div className="App">
		<div id="FormDiv">
			<Form>
				<Form.Group
		 controlId="markText">
					<FormLabel>Reporting Mark</FormLabel>
					<FormControl type="text" placeholder="AAAA" onChange={this.handleMarkChange}/>
				</Form.Group
		>
				<Form.Group
		 controlId="nameText">
					<FormLabel>Railroad Name</FormLabel>
					<FormControl type="text" placeholder="Railroad Name" onChange={this.handleNameChange}/>
				</Form.Group
		>
				<Button className="btn btn-primary" onClick={this.doSearch}>Search</Button>
				<Button className="btn btn-primary" onClick={() => axios.get(baseURL + "/allreportingmarks")
						.then((response) => this.setState({data: response.data}) )}>List All</Button>
			</Form>
		</div>
		<div id="ResultDiv">
			<RailroadList data={this.state.data} />
		</div>
      </div>
    );
  }
}

export default App;
