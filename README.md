# Railroad Information Lookup System (RaILS)

## What is this?

RaILS is a very basic web application for looking up [AAR](https://aar.org) reporting marks.  A running copy of this application is at https://railroadinfo.herokuapp.com/

## Web Service

The backend of the application is a Spring Boot web service using MyBatis for database interaction.

## User Interface

The frontend of the application is a React UI made with components from [pagedraw.io](https://pagedraw.io)

## Running locally

1. From the command line: `gradlew bootRun -Plocal`
2. Navigate to http://127.0.0.1:8080

